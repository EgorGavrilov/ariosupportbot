import os
import subprocess
import argparse

ngrok = 'ngrok.exe'

parser = argparse.ArgumentParser(description='ngrok server')
parser.add_argument('-t', '--token', type=str, help='ngrok user token')
parser.add_argument('-e', '--env', type=str, help='ngrok env variable')
parser.add_argument('-p', '--port', type=str, help='server port')


def main(args):
    token = os.environ[args.env] if args.env else args.token if args.token else None
    if token:
        with subprocess.Popen([ngrok, 'authtoken', token]) as auth:
            subprocess.Popen([ngrok, 'http', args.port], creationflags=subprocess.CREATE_NEW_CONSOLE)
    else:
        subprocess.Popen([ngrok, 'http', args.port], creationflags=subprocess.CREATE_NEW_CONSOLE)


if __name__ == '__main__':
    main(parser.parse_args())
