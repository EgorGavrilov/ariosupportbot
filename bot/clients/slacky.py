import os
from slack import WebClient

smart_client = WebClient(token=os.environ['SMART_API_TOKEN'])
support_client = WebClient(token=os.environ['SUPPORT_API_TOKEN'])
