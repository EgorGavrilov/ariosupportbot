import json
import string
from datetime import datetime
from hashlib import md5
from collections import defaultdict, namedtuple
from itertools import groupby

from flask import Blueprint, request, jsonify
from bot.clients.slacky import smart_client
from bot.models.standuply import (
    Message, Standup, MessageUserAnswers,
    RespondentDialogAnswer, RespondentDialogQuickAnswer,
    RespondentsDialogAnswerLater, RespondentsDialogAnswerNextTime,
    SkippedDialogAnswerData
)

Answer = namedtuple('Answer', 'user, body')
stopwords = ['нет', 'не', 'пас']
punct = string.punctuation + '• '

standuply_handler = Blueprint('standuply', __name__)
standups = defaultdict(Standup)


@standuply_handler.route('/standuply/hook', methods=['POST'])
def hook():
    md5h = md5(request.data).hexdigest()
    message = Message.from_dict(json.loads(request.data))
    standup = standups[message.scheduled_at]
    if md5h not in standup.hashes:
        standup.hashes.append(md5h)
        standup.messages.append(message)
        post_standup(standup)
    return jsonify(sucess=True)


def post_standup(standup: Standup):
    if standup.messages:
        questions = defaultdict(list)
        not_responded = []
        user_id = None
        channel = standup.messages[0].report_name
        for message in standup.messages:
            for result in message.result:
                if not isinstance(result.value, MessageUserAnswers):
                    raise AttributeError('Supported `MessageUserAnswers` only for standups')

                user_id = result.value.user_id
                data = result.value.data
                if isinstance(data, list) and all(isinstance(x, RespondentDialogAnswer) for x in data):
                    for datum in data:
                        questions[datum.question].append(Answer(user_id, datum.answer))
                elif isinstance(data, RespondentDialogQuickAnswer):
                    not_responded.append(Answer(user_id, data.answer))
                elif isinstance(data, RespondentsDialogAnswerLater) or isinstance(data, RespondentsDialogAnswerNextTime):
                    not_responded.append(Answer(user_id, f'Сказал, что ответит в {data.time}'))
                elif isinstance(data, SkippedDialogAnswerData):
                    not_responded.append(Answer(user_id, f'Не ответил вовсе'))

        not_responded = [response for response in not_responded
                         if all(answer.user != response.user for reply in questions.values() for answer in reply)]

        body = [f'_{channel}\'s daily-митинг!_ :happy_egor:']
        for question, replies in questions.items():
            body.append(f'*{question}*')
            replies_sorted = sorted(replies, key=lambda x: x.user)
            for key, group in groupby(replies_sorted, key=lambda x: x.user):
                user_replies = [line.strip(punct) for reply in group for line in reply.body.split('\n')
                                if line.strip(punct).lower() not in stopwords]
                for i, reply in enumerate(user_replies):
                    line = f'\t• {reply}'
                    if i == 0:
                        line += f' - <@{key}>'
                    body.append(line)
        if not_responded:
            body.append(f'*Пропустили митинг*')
            for reply in not_responded:
                body.append(f'\t• {reply.body} - <@{reply.user}>')

        if not standup.post:
            text = '\n'.join(body)
            standup.post = smart_client.chat_postMessage(channel=channel, text=text)
        else:
            if user_id:
                body.append(f'Последнее обновление - {datetime.now()} от <@{user_id}>')
            text = '\n'.join(body)
            smart_client.chat_update(channel=standup.post.data['channel'], ts=standup.post.data['ts'], text=text)
