import os

from datetime import time, datetime
from pytz import timezone
from bot.clients import slack_smart, slack_support
from slackeventsapi import SlackEventAdapter
from flask import Blueprint, Response
from http import HTTPStatus
from pandas.tseries.offsets import BDay


slack_events = Blueprint('slack_events', __name__)
slack_events_adapter = SlackEventAdapter(os.environ['SUPPORT_SIGNING_TOKEN'], '/slack/events', slack_events)

processed_messages = []

reply_text = [
    {
        "type": "section",
        "text": {
            "type": "mrkdwn",
            "text": "Привет, вам скоро ответят, а пока проверьте следующее"
        }
    },
    {
        "type": "divider"
    },
    {
        "type": "section",
        "text": {
            "type": "mrkdwn",
            "text": "Поищите ответ на ваш вопрос в ЧАВО на вики"
        }
    },
    {
        "type": "section",
        "text": {
            "type": "mrkdwn",
            "text": "Также проведите первичный анализ, воспользуйтесь загрузкой логов и трейсов в Elasticsearch"
        }
    },
    {
        "type": "section",
        "text": {
            "type": "mrkdwn",
            "text": "Убедитесь, что  сообщение отформатировано"
        }
    },
    {
        "type": "section",
        "text": {
            "type": "mrkdwn",
            "text": "Убедитесь, что вы приложили логи, трейсы и исходные данные (документы, модели ОИФ, правила и т.д.)"
        }
    },
    {
        "type": "divider"
    },
    {
        "type": "actions",
        "elements": [
            {
                "type": "button",
                "text": {
                    "type": "plain_text",
                    "text": "ЧАВО",
                    "emoji": True
                },
                "url": "https://wiki.npo-comp.ru/index.php?title=%D0%9A%D0%B0%D1%82%D0%B5%D0%B3%D0%BE%D1%80%D0%B8%D1%8F%3A%D0%A2%D0%B8%D0%BF%D0%BE%D0%B2%D1%8B%D0%B5_%D0%BF%D1%80%D0%BE%D0%B1%D0%BB%D0%B5%D0%BC%D1%8B_%D0%B8_%D1%80%D0%B5%D1%88%D0%B5%D0%BD%D0%B8%D1%8F_(%D0%A7%D0%90%D0%92%D0%9E)"
            },
            {
                "type": "button",
                "text": {
                    "type": "plain_text",
                    "text": "Elasticsearch",
                    "emoji": True
                },
                "url": "https://wiki.npo-comp.ru/index.php?title=%D0%9F%D0%BE%D1%80%D1%8F%D0%B4%D0%BE%D0%BA_%D0%B7%D0%B0%D0%B3%D1%80%D1%83%D0%B7%D0%BA%D0%B8_%D0%BB%D0%BE%D0%B3%D0%BE%D0%B2_%D0%B8_%D1%82%D1%80%D0%B5%D0%B9%D1%81%D0%BE%D0%B2_%D0%B2_Elasticsearch"
            },
            {
                "type": "button",
                "text": {
                    "type": "plain_text",
                    "text": "Форматирование",
                    "emoji": True
                },
                "url": "https://slack.com/intl/en-ru/help/articles/202288908-Format-your-messages"
            },
            {
                "type": "button",
                "text": {
                    "type": "plain_text",
                    "text": "Вложения",
                    "emoji": True
                },
                "url": "https://slack.com/intl/en-ru/help/articles/204145658-Create-a-snippet"
            }
        ]
    }
]


def set_response_message():
    tz = timezone('Europe/Samara')
    now = datetime.now(tz=tz)
    bdays = BDay()
    is_business_day = bdays.is_on_offset(now)
    if not is_business_day:
        reply_text[0]['text']['text'] = 'Привет, сотрудники дерутся за право ответить в нерабочий день, победивший сотрудник ответит в понедельник, а пока проверьте следующее:'
    elif now.time() < time(9, 0, 0) or now.time() > time(19, 0, 0):
        reply_text[0]['text']['text'] = 'Привет, сейчас скорее всего никого нет, ожидайте ответ в рабочее время, а пока проверьте следующее:'
    else:
        reply_text[0]['text']['text'] = 'Привет, вам скоро ответят, а пока проверьте следующее:'


@slack_events_adapter.on('message')
def handle_message(event_data):
    message = event_data['event']
    event_id = event_data['event_id']
    if event_id in processed_messages:
        return Response(status=HTTPStatus.OK)

    processed_messages.append(event_id)
    if message.get('thread_ts') is None and message.get('subtype') is None:
        set_response_message()

        if (message.get('ts') is not None):
            print(message)
            slack_support.chat_postMessage(channel=message['channel'], blocks=reply_text, thread_ts=message['ts'])

        user = slack_support.users_info(user=message['user'])
        response_text = message['text'].replace('\n', '\n>')
        link = slack_support.chat_getPermalink(channel=message['channel'], message_ts=message['ts'])
        response = f'> *{user.data["user"]["real_name"]}@ario-support*\n> {response_text}\n>{link["permalink"]}'
        slack_smart.chat_postMessage(channel='#ario-problems', text=response)

    return Response(status=HTTPStatus.OK)

@slack_events_adapter.on("error")
def error_handler(err):
    print("ERROR: " + str(err))
