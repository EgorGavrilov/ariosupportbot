from dataclasses import dataclass, field
from typing import Optional, Any, Union, List

from slack.web.slack_response import SlackResponse
from .converters import from_none, from_str, from_bool, from_int, from_union, from_list, to_bool, to_class


@dataclass
class SlackMessageAttachmentField:
    title: Optional[str] = None
    value: Optional[str] = None
    short: Optional[bool] = None

    @staticmethod
    def from_dict(obj: Any) -> 'SlackMessageAttachmentField':
        assert isinstance(obj, dict)
        title = from_union([from_str, from_none], obj.get('title'))
        value = from_union([from_str, from_none], obj.get('value'))
        short = from_union([from_bool, from_none], obj.get('short'))
        return SlackMessageAttachmentField(title, value, short)

    def to_dict(self) -> dict:
        result: dict = {}
        result['title'] = from_union([from_str, from_none], self.title)
        result['value'] = from_union([from_str, from_none], self.value)
        result['short'] = from_union([to_bool, from_none], self.short)
        return result


@dataclass
class SlackMessageAttachment:
    fallback: Optional[str] = None
    color: Optional[str] = None
    pretext: Optional[str] = None
    author_name: Optional[str] = None
    author_link: Optional[str] = None
    author_icon: Optional[str] = None
    title: Optional[str] = None
    title_link: Optional[str] = None
    text: Optional[str] = None
    fields: Optional[List[SlackMessageAttachmentField]] = None
    image_url: Optional[str] = None
    thumb_url: Optional[str] = None
    footer: Optional[str] = None
    footer_icon: Optional[str] = None
    ts: Optional[int] = None
    mrkdwn_in: Optional[List[str]] = None

    @staticmethod
    def from_dict(obj: Any) -> 'SlackMessageAttachment':
        assert isinstance(obj, dict)
        fallback = from_union([from_str, from_none], obj.get('fallback'))
        color = from_union([from_str, from_none], obj.get('color'))
        pretext = from_union([from_str, from_none], obj.get('pretext'))
        author_name = from_union([from_str, from_none], obj.get('authorName'))
        author_link = from_union([from_str, from_none], obj.get('authorLink'))
        author_icon = from_union([from_str, from_none], obj.get('authorIcon'))
        title = from_union([from_str, from_none], obj.get('title'))
        title_link = from_union([from_str, from_none], obj.get('titleLink'))
        text = from_union([from_str, from_none], obj.get('text'))
        fields = from_union([lambda x: from_list(SlackMessageAttachmentField.from_dict, x), from_none],
                            obj.get('fields'))
        image_url = from_union([from_str, from_none], obj.get('imageUrl'))
        thumb_url = from_union([from_str, from_none], obj.get('thumbUrl'))
        footer = from_union([from_str, from_none], obj.get('footer'))
        footer_icon = from_union([from_str, from_none], obj.get('footerIcon'))
        ts = from_union([from_int, from_none], obj.get('ts'))
        mrkdwn_in = from_union([lambda x: from_list(from_str, x), from_none], obj.get('mrkdwnIn'))
        return SlackMessageAttachment(fallback, color, pretext, author_name, author_link, author_icon, title,
                                      title_link, text, fields, image_url, thumb_url, footer, footer_icon,
                                      ts, mrkdwn_in)

    def to_dict(self) -> dict:
        result: dict = {}
        result['fallback'] = from_union([from_str, from_none], self.fallback)
        result['color'] = from_union([from_str, from_none], self.color)
        result['pretext'] = from_union([from_str, from_none], self.pretext)
        result['authorName'] = from_union([from_str, from_none], self.author_name)
        result['authorLink'] = from_union([from_str, from_none], self.author_link)
        result['authorIcon'] = from_union([from_str, from_none], self.author_icon)
        result['title'] = from_union([from_str, from_none], self.title)
        result['titleLink'] = from_union([from_str, from_none], self.title_link)
        result['text'] = from_union([from_str, from_none], self.text)
        result['fields'] = from_union([lambda x: to_class(SlackMessageAttachmentField, x), from_none], self.fields)
        result['imageUrl'] = from_union([from_str, from_none], self.image_url)
        result['thumbUrl'] = from_union([from_str, from_none], self.thumb_url)
        result['footer'] = from_union([from_str, from_none], self.footer)
        result['footerIcon'] = from_union([from_str, from_none], self.footer_icon)
        result['ts'] = from_union([from_int, from_none], self.ts)
        result['mrkdwnIn'] = from_union([lambda x: from_list(from_str, x), from_none], self.mrkdwn_in)
        return result


@dataclass
class MessageCounts:
    option: Optional[str] = None
    count: Optional[int] = None

    @staticmethod
    def from_dict(obj: Any) -> 'MessageCounts':
        assert isinstance(obj, dict)
        option = from_union([from_str, from_none], obj.get('option'))
        count = from_union([from_int, from_none], obj.get('count'))
        return MessageCounts(option, count)

    def to_dict(self) -> dict:
        result: dict = {}
        result['option'] = from_union([from_str, from_none], self.option)
        result['count'] = from_union([from_int, from_none], self.count)
        return result


@dataclass
class MessageSurveySummary:
    question: Optional[str] = None
    is_anon: Optional[bool] = None
    counts: Optional[List[MessageCounts]] = None
    image_url: Optional[str] = None

    @staticmethod
    def from_dict(obj: Any) -> 'MessageSurveySummary':
        assert isinstance(obj, dict)
        question = from_union([from_str, from_none], obj.get('question'))
        is_anon = from_union([from_bool, from_none], obj.get('isAnonymous'))
        counts = from_union([lambda x: from_list(MessageCounts.from_dict, x), from_none], obj.get('counts'))
        image_url = from_union([from_str, from_none], obj.get('imageUrl'))
        return MessageSurveySummary(question, is_anon, counts, image_url)

    def to_dict(self) -> dict:
        result: dict = {}
        result['question'] = from_union([from_str, from_none], self.question)
        result['isAnonymous'] = from_union([to_bool, from_none], self.is_anon)
        result['counts'] = from_union([lambda x: to_class(MessageCounts, x), from_none], self.counts)
        result['imageUrl'] = from_union([from_str, from_none], self.image_url)
        return result


@dataclass
class MessageRequestResult:
    request_name: Optional[str] = None
    attachment: Optional[SlackMessageAttachment] = None

    @staticmethod
    def from_dict(obj: Any) -> 'MessageRequestResult':
        assert isinstance(obj, dict)
        request_name = from_union([from_str, from_none], obj.get('requestName'))
        attachment = from_union([SlackMessageAttachment.from_dict, from_none], obj.get('attachment'))
        return MessageRequestResult(request_name, attachment)

    def to_dict(self) -> dict:
        result: dict = {}
        result['requestName'] = from_union([from_str, from_none], self.request_name)
        result['attachment'] = from_union([SlackMessageAttachment.to_dict, from_none], self.attachment)
        return result


@dataclass
class RespondentDialogQuickAnswer:
    answer: Optional[str] = None

    @staticmethod
    def from_dict(obj: Any) -> 'RespondentDialogQuickAnswer':
        assert isinstance(obj, dict)
        answer = from_union([from_str, from_none], obj.get('answer'))
        return RespondentDialogQuickAnswer(answer)

    def to_dict(self) -> dict:
        result: dict = {}
        result['answer'] = from_union([from_str, from_none], self.answer)
        return result


@dataclass
class RespondentsDialogAnswerNextTime:
    time: Optional[str] = None

    @staticmethod
    def from_dict(obj: Any) -> 'RespondentsDialogAnswerNextTime':
        assert isinstance(obj, dict)
        time = from_union([from_str, from_none], obj.get('time'))
        return RespondentsDialogAnswerNextTime(time)

    def to_dict(self) -> dict:
        result: dict = {}
        result['time'] = from_union([from_str, from_none], self.time)
        return result


@dataclass
class RespondentsDialogAnswerLater:
    time: Optional[str] = None
    answer_at: Optional[int] = None

    @staticmethod
    def from_dict(obj: Any) -> 'RespondentsDialogAnswerLater':
        assert isinstance(obj, dict)
        time = from_union([from_str, from_none], obj.get('time'))
        answer_at = from_union([from_int, from_none], obj.get('willAnswerAt'))
        return RespondentsDialogAnswerLater(time, answer_at)

    def to_dict(self) -> dict:
        result: dict = {}
        result['time'] = from_union([from_str, from_none], self.time)
        result['willAnswerAt'] = from_union([from_int, from_none], self.answer_at)
        return result


@dataclass
class SkippedDialogAnswerData:
    @staticmethod
    def from_dict(obj: Any) -> 'SkippedDialogAnswerData':
        return SkippedDialogAnswerData()

    def to_dict(self) -> dict:
        return {}


@dataclass
class RespondentDialogAnswer:
    question: Optional[str] = None
    type: Optional[str] = None
    answer: Optional[str] = None
    answer_id: Optional[str] = None

    @staticmethod
    def from_dict(obj: Any) -> 'RespondentDialogAnswer':
        assert isinstance(obj, dict)
        question = from_union([from_str, from_none], obj.get('question'))
        type = from_union([from_str, from_none], obj.get('type'))
        answer = from_union([from_str, from_none], obj.get('answer'))
        answer_id = from_union([from_str, from_none], obj.get('answerId'))
        return RespondentDialogAnswer(question, type, answer, answer_id)

    def to_dict(self) -> dict:
        result: dict = {}
        result['question'] = from_union([from_str, from_none], self.question)
        result['type'] = from_union([from_str, from_none], self.type)
        result['answer'] = from_union([from_str, from_none], self.answer)
        result['answerId'] = from_union([from_str, from_none], self.answer_id)
        return result


@dataclass
class MessageUserAnswers:
    user_id: Optional[str] = None
    user_name: Optional[str] = None
    type: Optional[str] = None
    data: Optional[
        Union[
            List[RespondentDialogAnswer], RespondentDialogQuickAnswer, RespondentsDialogAnswerLater,
            RespondentsDialogAnswerNextTime, SkippedDialogAnswerData
        ]
    ] = None

    @staticmethod
    def from_dict(obj: Any) -> 'MessageUserAnswers':
        assert isinstance(obj, dict)
        user_id = from_union([from_str, from_none], obj.get('userId'))
        user_name = from_union([from_str, from_none], obj.get('userName'))
        type = from_union([from_str, from_none], obj.get('type'))
        data = from_union([lambda x: from_list(RespondentDialogAnswer.from_dict, x),
                           RespondentDialogQuickAnswer.from_dict,
                           RespondentsDialogAnswerLater.from_dict,
                           RespondentsDialogAnswerNextTime.from_dict,
                           SkippedDialogAnswerData.from_dict],
                          obj.get('data'))
        return MessageUserAnswers(user_id, user_name, type, data)

    def to_dict(self) -> dict:
        result: dict = {}
        result['userId'] = from_union([from_str, from_none], self.user_id)
        result['userName'] = from_union([from_str, from_none], self.user_name)
        result['type'] = from_union([from_str, from_none], self.type)
        result['data'] = from_union([lambda x: to_class(RespondentDialogAnswer, x),
                                     RespondentDialogQuickAnswer.to_dict,
                                     RespondentsDialogAnswerLater.to_dict,
                                     RespondentsDialogAnswerNextTime.to_dict,
                                     SkippedDialogAnswerData.to_dict],
                                    self.data)
        return result


@dataclass
class MessageResult:
    type: Optional[str] = None
    value: Optional[Union[MessageUserAnswers, MessageRequestResult, MessageSurveySummary]] = None

    @staticmethod
    def from_dict(obj: Any) -> 'MessageResult':
        assert isinstance(obj, dict)
        type = from_union([from_str, from_none], obj.get('type'))
        value = from_union([MessageUserAnswers.from_dict, MessageRequestResult.from_dict,
                            MessageSurveySummary.from_dict, from_none],
                           obj.get('value'))
        return MessageResult(type, value)

    def to_dict(self) -> dict:
        result: dict = {}
        result['type'] = from_union([from_str, from_none], self.type)
        result['value'] = from_union([MessageUserAnswers.to_dict, MessageRequestResult.to_dict,
                                      MessageSurveySummary.to_dict, from_none],
                                     self.value)
        return result


@dataclass
class Subject:
    id: Optional[str] = None
    type: Optional[str] = None
    title: Optional[str] = None

    @staticmethod
    def from_dict(obj: Any) -> 'Subject':
        assert isinstance(obj, dict)
        id = from_union([from_str, from_none], obj.get('id'))
        type = from_union([from_str, from_none], obj.get('type'))
        title = from_union([from_str, from_none], obj.get('title'))
        return Subject(id, type, title)

    def to_dict(self) -> dict:
        result: dict = {}
        result['id'] = from_union([from_str, from_none], self.id)
        result['type'] = from_union([from_str, from_none], self.type)
        result['title'] = from_union([from_str, from_none], self.title)
        return result


@dataclass
class Message:
    report_name: Optional[str] = None
    scheduled_at: Optional[int] = None
    team_id: Optional[str] = None
    subject: Optional[Subject] = None
    is_async: Optional[bool] = None
    is_finished: Optional[bool] = None
    result: Optional[List[MessageResult]] = None

    @staticmethod
    def from_dict(obj: Any) -> 'Message':
        assert isinstance(obj, dict)
        report_name = from_union([from_str, from_none], obj.get('reportName'))
        scheduled_at = from_union([from_int, from_none], obj.get('scheduledAt'))
        team_id = from_union([from_str, from_none], obj.get('teamId'))
        subject = from_union([Subject.from_dict, from_none], obj.get('subject'))
        is_async = from_union([from_bool, from_none], obj.get('isAsyncResults'))
        is_finished = from_union([from_bool, from_none], obj.get('isReportFinished'))
        result = from_union([MessageResult.from_dict, lambda x: from_list(MessageResult.from_dict, x), from_none],
                            obj.get('result'))
        if result is not None and not isinstance(result, list):
            result = [result]
        return Message(report_name, scheduled_at, team_id, subject, is_async, is_finished, result)

    def to_dict(self) -> dict:
        result: dict = {}
        result['reportName'] = from_union([from_str, from_none], self.report_name)
        result['scheduledAt'] = from_union([from_int, from_none], self.scheduled_at)
        result['teamId'] = from_union([from_str, from_none], self.team_id)
        result['subject'] = from_union([Subject.to_dict, from_none], self.subject)
        result['isAsyncResults'] = from_union([to_bool, from_none], self.is_async)
        result['isReportFinished'] = from_union([to_bool, from_none], self.is_finished)
        result['result'] = from_union([lambda x: to_class(MessageResult, x), from_none], self.result)
        return result


@dataclass
class Standup:
    messages: Optional[List[Message]] = field(default_factory=list)
    post: Optional[SlackResponse] = None
    hashes: Optional[List[str]] = field(default_factory=list)
