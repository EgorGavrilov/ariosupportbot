from flask import Flask
from .blueprints import slack_blueprint, standaply_blueprint


app = Flask(__name__)
app.register_blueprint(slack_blueprint)
app.register_blueprint(standaply_blueprint)
